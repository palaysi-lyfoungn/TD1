#include <stdio.h>

struct Personne {
    char *nom;
    int age;
    float taille;
};

struct Famille {
    char *nom;
    struct Personne pere;
    struct Personne mere;
    int nb_enfants;
    struct Personne enfants[2]; // Mettez la taille du tableau d'enfants à la valeur désirée
};

int age_total(struct Famille f);

int age_total(struct Famille f) {
    int total = f.pere.age + f.mere.age;
    for (int i = 0; i < f.nb_enfants; i++) {
        total += f.enfants[i].age;
    }
    return total;
}

