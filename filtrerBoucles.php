<?php

/**
 * Donnée : un source C indenté par la commande indent en entrée
 * Résultat : affiche le corps des boucles for, while et do-while du source en sortie
 */

while(!feof(STDIN)){

    do{
        $ligne=fgets(STDIN);
        $bouclewhile=preg_match("/while \(.*\)$/",$ligne)==1;
        $bouclefor=preg_match("/for \(.*\)$/",$ligne)==1;
        $boucledo=preg_match("/do$/",$ligne)==1;
        $boucle=($bouclewhile or $bouclefor or $boucledo);
    }while(!$boucle && !feof(STDIN));

    echo $ligne;
    
    $finBloc=false;
    $cpt=0;
    while(!$finBloc && !feof(STDIN)){
        $ligne=fgets(STDIN);
        echo $ligne;
        if (preg_match("/{/",$ligne)==1)
            $cpt++;
        else if (preg_match("/}/",$ligne)==1)
            $cpt--;
        if ($cpt<=0 or $ligne===false)
            $finBloc=true;
    }
    
}
