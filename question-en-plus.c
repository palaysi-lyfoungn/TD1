
#include <stdio.h>
#define MESSAGE "super\n"



int main() {
    printf(MESSAGE);
    
    int tab[3];
    tab[0] = 1;
    tab[1] = 2;
    printf("a: %i, b: %i, c: %i\n", tab[0], tab[1], tab[2]);

    // Y o u p i \0 n donc 7


    int a, *b, c;
    a = 12;
    b = &a;
    *b = a + 1;
    printf("a = %i, b = %lu\n", a, (long unsigned) b);

    float tab2[3];
    printf("premier adresse = %lu, deuxieme adresse = %lu\n", (long unsigned) tab2[0], (long unsigned) tab2[1]);
    // une adresse memoire d'un int vaut 4096
    // une adresse memoir d'un float vaut 0

    int t[2];
    printf("Premiere adresse: %lu,\nseconde adresse: %lu\n",
    (long unsigned) t,
    (long unsigned) (t+1));

}